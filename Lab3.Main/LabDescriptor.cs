﻿using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(void);
        public static Type I2 = typeof(void);
        public static Type I3 = typeof(void);

        public static Type Component = typeof(void);

        public delegate object GetInstance(object component);

        public static GetInstance GetInstanceOfI1 = null;
        public static GetInstance GetInstanceOfI2 = null;
        public static GetInstance GetInstanceOfI3 = null;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(void);
        public static Type MixinFor = typeof(void);

        #endregion
    }
}
